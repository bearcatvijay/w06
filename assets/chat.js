var socket = io()  //use our helpful socket.io library to create an event emitter

new Vue({
  el: '#app',
  data: {
    messages: [],
    fromUser: ''
  },
  mounted () {
    let _this = this
    socket.on('chatMessage', function (from, msg) {
      var me = this.fromUser
      var color = (from === me) ? 'green' : '#009afd'
      var from = (from === me) ? 'Me' : from
      _this.messages.push({
        from: from,
        msg: msg,
        color: color
      })
     })

     // how to react to a notifyUser event.................
    socket.on('notifyUser', function (user) {
      var me = _this.fromUser
      if (user !== me) {
        $('#notifyUser').text(user + ' is typing ...')
      }
      // 10 seconds after typing stops, set the notify text to an empty string
      setTimeout(function () { $('#notifyUser').text('') }, 5000)
     })
     
     _this.fromUser = this.makeid()
     // emit a chatMessage event from the System along with a message
     socket.emit('chatMessage', 'System', _this.fromUser + ' has joined the discussion')
  },
  methods: {
    makeid() {
      return '_' + Math.random().toString(36).substr(2, 9);
    },
    notifyTyping() {
      socket.emit('notifyUser', this.fromUser)
     },
     submitfunction() {
      let from = this.fromUser
      let message = $('#m').val()
      if (message != '') {
        socket.emit('chatMessage', from, message)// can handle more than 1 item ...
      }
      // what language and selector is used below?
      // set the value to an empty string and
      // focus on the message box again
      // return false so the form cancels the submit before sending to the server
      $('#m').val('').focus()
      return false
    }
  }
})