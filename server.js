var express = require('express');  
var app = express();  

var server = require('http').createServer(app);  
var io = require('socket.io')(server);

// app.use(express.static(__dirname + '/node_modules'));  
app.use(express.static('assets'))

app.get('/', function(req, res,next) {  
    res.sendFile(__dirname + '/index.html');
});


// on a connection event, act as follows (socket interacts with client)
io.on('connection', function (socket) {
  socket.on('chatMessage', function (from, msg) {  // on getting a chatMessage event
    io.emit('chatMessage', from, msg)  // emit it to all connected clients
  })
  socket.on('notifyUser', function (user) {  // on getting a notifyUser event
    io.emit('notifyUser', user)  // emit to all
  })
 })
console.log('The server is running at http://127.0.0.1:8080') 
server.listen(8080);  
